@extends('admin.layouts.master')
@section('title')
Utilities
@endsection

@section('content')
<div class="mt-4">
	<h2>Enable/Disable Stores</h2>
	<a href="{{ route('admin.utility.toggleStoreStatus', 'enable') }}" class="btn btn-dark btn-lg">Enable all Stores</a>
	<br><br>
	<a href="{{ route('admin.utility.toggleStoreStatus', 'disable') }}" class="btn btn-dark btn-lg">Disable all Stores</a>
	<hr>
</div>

<footer>
	Semoga Usaha Anda Makin Sukses.
</footer>
@endsection